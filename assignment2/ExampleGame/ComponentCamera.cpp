#include "ComponentCamera.h"
#include "Transform.h"
#include "GameObject.h"
#include "GameObjectManager.h"

ComponentCamera::ComponentCamera()
{
	m_pCamera = new Common::SceneCamera(45.0f, 1280.0f / 720.0f, 0.1f, 1000.0f, glm::vec3(0.0f, 5.0f, 0.0f), glm::vec3(0.0f,5.0f,0.0f), glm::vec3(0.0f,1.0f,0.0f));
}

void ComponentCamera::SetCamera(Common::SceneCamera* p_pCamera)
{
	m_pCamera = p_pCamera;
}


void ComponentCamera::Update(float p_fDelta) 
{
	Common::Transform objectTransform = this->GetGameObject()->GetTransform();
	if (this->GetGameObject()->GetGUID() == "character")
	{
		float fObjectRotation = (objectTransform.GetRotation().y-40.0f) * 3.142 / 180;
		m_pCamera->SetPos(objectTransform.GetTranslation() + glm::vec3((-sin(fObjectRotation) * 15.0f), 7.0f, (-cos(fObjectRotation) * 15.0f)));
		m_pCamera->SetTarget(objectTransform.GetTranslation() + glm::vec3(0.0f, 7.0f, 0.0f));
	}
	else if (this->GetGameObject()->GetGUID() == "lamp")
	{
		Common::GameObject* pCharacter = this->GetGameObject()->GetManager()->GetGameObject("character");
		m_pCamera->SetTarget(pCharacter->GetTransform().GetTranslation());
	}
}