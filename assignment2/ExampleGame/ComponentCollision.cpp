#include "ComponentCollision.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "ComponentTimerLogic.h"
#include <algorithm>
#include "EventManager.h"
#include "EventObjectCollision.h"

using namespace week2;
using namespace std;

Common::ComponentBase* ComponentCollision::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_CollisionSphere") == 0);
	ComponentCollision* pCollisionComponent = new ComponentCollision();

	// Iterate elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();
		if (strcmp(szNodeName, "Radius") == 0)
		{
			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			const char* szValue = pElement->Attribute("value");
			if (szValue == NULL)
			{
				delete pCollisionComponent;
				return NULL;
			}
			
			float fValue = atof(szValue);
			pCollisionComponent->SetRadius(fValue);
		}

		pChildNode = pChildNode->NextSibling();
	}

	return pCollisionComponent;
}


void ComponentCollision::Update(float p_fDelta)
{
	//Common::GameObject *pCharacter = this->GetGameObject()->GetManager()->GetGameObject("character");
	//if (pCharacter == this->GetGameObject()) return;
	//Common::Transform& transform = this->GetGameObject()->GetTransform();
	//glm::vec3 distance = transform.GetTranslation() - pCharacter->GetTransform().GetTranslation();
	//float dis = sqrt( distance.x*distance.x + distance.z*distance.z);
	//if (dis < m_fBoundingRadius)
	//{
	//	EventManager::Instance()->QueueEvent(new EventObjectCollision(pCharacter, this->GetGameObject()));

	//	//printf("yes");
	//	//ComponentTimerLogic *pTimerLogic = static_cast<ComponentTimerLogic*>(this->GetGameObject()->GetManager()->GetGameObject("timer")->GetComponent("GOC_TimerLogic"));
	//	////pTimerLogic->m_lCoinList.erase(this->GetGameObject());
	//
	//	//std::vector<Common::GameObject*>::iterator it = std::find(pTimerLogic->m_lCoinList.begin(), pTimerLogic->m_lCoinList.end(), this->GetGameObject());
	//	//if (it != pTimerLogic->m_lCoinList.end())
	//	//{
	//	//	pTimerLogic->m_lCoinList.erase(it);
	//	//}	
	//
	//	//this->GetGameObject()->GetManager()->m_lRemoveGOList.push_back(this->GetGameObject());
	//}
}