#include "TTextTable.h"

vector<string> TTextTable::m_table;
int TTextTable::m_iLanguage = 1;

void TTextTable::Load(char* p_str)
{
	ifstream Stream (p_str);
	string line;
	string read;
	while( !Stream.eof() )
	{	
		std::getline( Stream, line);
		std::stringstream LineStream(line);

		string w1;
		std::getline( LineStream, w1, '\t');
		if (read == "ID") continue;
		m_table.push_back(w1);

		string w2;
		std::getline( LineStream, w2, '\t');
		m_table.push_back(w2);

		string w3;
		std::getline( LineStream, w3, '\t');
		m_table.push_back(w3);
	}
	Stream.close();
}

string TTextTable::GetString(char* p_str)
{
	for (int i=0; i<m_table.size();i++)
	{
		string word= m_table.at(i);
		if (p_str == word)
			return m_table.at(i+m_iLanguage);
	}
	return "";
}


void TTextTable::SetLanguage(char* p_str)
{
	if (p_str == "English")
		m_iLanguage = 1;
	else if (p_str == "French")
		m_iLanguage = 2;
}