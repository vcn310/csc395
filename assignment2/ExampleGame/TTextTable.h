
#include <sstream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class TTextTable
{
public:
	static void Load(char* p_str);
	static void SetLanguage(char* p_str);
	static string GetString(char* p_str);

private:
	static vector<string> TTextTable::m_table;
	static int m_iLanguage;
};