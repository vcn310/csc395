//------------------------------------------------------------------------
// GameObjectManager
//
// Created:	2012/12/26
// Author:	Carel Boers
//	
// Manages the collection of Game Objects used by a game.
//------------------------------------------------------------------------

#include "GameObjectManager.h"
#include "ComponentRenderable.h"
#include "assignment2\ExampleGame\EventManager.h"
#include "assignment2\ExampleGame\ComponentCollision.h"
#include "assignment2\ExampleGame\EventObjectCollision.h"

using namespace Common;

//------------------------------------------------------------------------------
// Method:    GameObjectManager
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
GameObjectManager::GameObjectManager()
{
	//EventListener eventListener1 = std::tr1::bind(&GameObjectManager::HandleObjectRemoveEvent, this, std::tr1::placeholders::_1);
	//EventManager::Instance()->AddListener(Event_CoinCollected, eventListener1);
}

//------------------------------------------------------------------------------
// Method:    ~GameObjectManager
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
GameObjectManager::~GameObjectManager()
{
	assert(m_mGOMap.size() == 0);
}

//------------------------------------------------------------------------------
// Method:    CreateGameObject
// Returns:   GameObject*
// 
// Factory creation method for empty GameObjects.
//------------------------------------------------------------------------------
GameObject* GameObjectManager::CreateGameObject()
{
	GameObject* pGO = new GameObject(this);
	m_mGOMap.insert(std::make_pair<std::string, GameObject*>(pGO->GetGUID(), pGO));
	return pGO;
}

//------------------------------------------------------------------------------
// Method:    DestroyGameObject
// Parameter: GameObject * p_pGameObject
// Returns:   void
// 
// Destroys the given Game Object and removes it from internal map of all Game
// Objects.
//------------------------------------------------------------------------------
void GameObjectManager::DestroyGameObject(GameObject* p_pGameObject)
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.find(p_pGameObject->GetGUID());
	if (it != m_mGOMap.end())
	{
		pGO = (GameObject*)it->second;
		delete pGO;
		m_mGOMap.erase(it);
	}
}

//------------------------------------------------------------------------------
// Method:    DestroyAllGameObjects
// Returns:   void
// 
// Clears the internal map of all GameObjects and deletes them.
//------------------------------------------------------------------------------
void GameObjectManager::DestroyAllGameObjects()
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;
		delete pGO;
	}
	m_mGOMap.clear();
}

//------------------------------------------------------------------------------
// Method:    GetGameObject
// Parameter: const std::string & p_strGOGUID
// Returns:   GameObject*
// 
// Returns a GameObject mapped to by the given GUID. Returns NULL if no GO is 
// found.
//------------------------------------------------------------------------------
GameObject* GameObjectManager::GetGameObject(const std::string &p_strGOGUID)
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.find(p_strGOGUID);
	if (it != m_mGOMap.end())
	{
		pGO = (GameObject*)it->second;
	}

	return pGO;
}

//------------------------------------------------------------------------------
// Method:    SetGameObjectGUID
// Parameter: GameObject * p_pGameObject
// Parameter: const std::string &p_strGOGUID
// Returns:   bool
// 
// Changes the name of the given GUID and updates the internal mapping. Returns 
// true for a successful rename, false on naming collision.
//------------------------------------------------------------------------------
bool GameObjectManager::SetGameObjectGUID(GameObject* p_pGameObject, const std::string &p_strGOGUID)
{
	GameObject* pGO = this->GetGameObject(p_strGOGUID);
	if (pGO)
	{
		// GUID already taken
		return false;
	}

	GameObjectMap::iterator it = m_mGOMap.find(p_pGameObject->GetGUID());
	if (it == m_mGOMap.end())
	{
		// No mapping - Game Object wasn't created from this GameObjectManager instance
		return false;
	}

	// Erase the old mapping, update the GUID and add it back
	m_mGOMap.erase(it);
	p_pGameObject->SetGUID(p_strGOGUID);
	m_mGOMap.insert(std::make_pair<std::string, GameObject*>(p_pGameObject->GetGUID(), p_pGameObject));
	return true;
}


//------------------------------------------------------------------------------
// Method:    CreateGameObject
// Parameter: const std::string & p_strGameObject
// Returns:   GameObject*
// 
// Factory creation method for GameObjects defined by an XML file. Returns NULL
// if the GameObject couldn't be properly constructed.
//------------------------------------------------------------------------------
GameObject* GameObjectManager::CreateGameObject(const std::string& p_strGameObject)
{
	// Load the document and return NULL if it fails to parse
	TiXmlDocument doc(p_strGameObject.c_str());
	if (doc.LoadFile() == false)
	{
		return NULL;
	}

	// Look for the root "GameObject" node and return NULL if it's missing
	TiXmlNode* pNode = doc.FirstChild("GameObject");
	if (pNode == NULL)
	{
		return NULL;
	}

	// Create the game object
	GameObject* pGO = new GameObject(this);
	m_mGOMap.insert(std::make_pair<std::string, GameObject*>(pGO->GetGUID(), pGO));

	// Iterate components in the XML and delegate to factory methods to construct components
	TiXmlNode* pComponentNode = pNode->FirstChild();
	while (pComponentNode != NULL)
	{
		const char* szComponentName = pComponentNode->Value();
		ComponentFactoryMap::iterator it = m_mComponentFactoryMap.find(szComponentName);
		if (it != m_mComponentFactoryMap.end())
		{
			ComponentFactoryMethod factory = it->second;
			ComponentBase* pComponent = factory(pComponentNode);
			if (pComponent != NULL)
			{
				pGO->AddComponent(pComponent);
			}
		}

		pComponentNode = pComponentNode->NextSibling();
	}
		
	return pGO;
}

//------------------------------------------------------------------------------
// Method:    RegisterComponentFactory
// Parameter: const std::string & p_strComponentId
// Parameter: ComponentFactoryMethod p_factoryMethod
// Returns:   void
// 
// Registers a component factory for a given component Id.
//------------------------------------------------------------------------------
void GameObjectManager::RegisterComponentFactory(const std::string& p_strComponentId, ComponentFactoryMethod p_factoryMethod)
{
	ComponentFactoryMap::iterator it = m_mComponentFactoryMap.find(p_strComponentId);
	if (it != m_mComponentFactoryMap.end())
	{
		return; // Already registered
	}

	// Insert it
	m_mComponentFactoryMap.insert(std::make_pair<std::string, ComponentFactoryMethod>(p_strComponentId, p_factoryMethod));
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Updates all the GameObjects*
//------------------------------------------------------------------------------
void GameObjectManager::Update(float p_fDelta)
{
	while (m_lRemoveGOList.size()>0)
	{
		GameObject *pRemoveObject = m_lRemoveGOList.at(0);
		m_lRemoveGOList.erase(m_lRemoveGOList.begin());
		this->DestroyGameObject(pRemoveObject);
	}

	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;
		pGO->Update(p_fDelta);
	}
	CheckCollision();
}

//------------------------------------------------------------------------------
// Method:    SyncTransforms
// Returns:   void
// 
// Syncs the GameObject transform to the renderable component (if one 
// exists for the given GameObject).
//------------------------------------------------------------------------------
void GameObjectManager::SyncTransforms()
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;
	
		// If this GO has a Renderable component, sync it's transform from the parent GO
		Common::ComponentBase* pComponent = pGO->GetComponent("GOC_Renderable");
		if (pComponent)
		{
			ComponentRenderable* pRenderable = static_cast<ComponentRenderable*>(pComponent);
			pRenderable->SyncTransform();
		}
	}
}

void GameObjectManager::AddRemovedObject(GameObject *p_pObject)
{
	m_lRemoveGOList.push_back(p_pObject);
}

void GameObjectManager::CheckCollision()
{
	GameObject* pCharacter = NULL;
	GameObjectMap::iterator it1 = m_mGOMap.find("character");
	if (it1 != m_mGOMap.end())
	{
		pCharacter = (GameObject*)it1->second;
	}


	GameObject* pCoin = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pCoin = (GameObject*)it->second;
		if (pCoin->GetGUID() != "character")
		{
			week2::ComponentCollision* pCollison1 = static_cast<week2::ComponentCollision*>(pCoin->GetComponent("GOC_CollisionSphere"));
			if (pCollison1)
			{
				Transform &coinTransform = pCoin->GetTransform();
				glm::vec3 distance1 = coinTransform.GetTranslation() - pCharacter->GetTransform().GetTranslation();
				float dis = sqrt( distance1.x*distance1.x + distance1.z*distance1.z); 

			
				week2::ComponentCollision* pCollison2 = static_cast<week2::ComponentCollision*>(pCharacter->GetComponent("GOC_CollisionSphere"));

				float radius = pCollison1->GetRadius() + pCollison2->GetRadius();
				if (dis<radius)
					EventManager::Instance()->QueueEvent(new EventObjectCollision(pCharacter, pCoin));
			}
		}
	}
}