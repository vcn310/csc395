//------------------------------------------------------------------------
// GameObjectManager
//
// Created:	2012/12/26
// Author:	Carel Boers
//	
// Manages the collection of Game Objects used by a game.
//------------------------------------------------------------------------

#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include "GameObject.h"
#include <map>
#include <vector>
#include "tinyxml\tinyxml.h"

namespace Common
{
	class GameObjectManager
	{
	public:
		// Typedef for convenience
		typedef std::map<std::string, GameObject*> GameObjectMap;
		typedef std::vector<GameObject*> GameObjectList;		

		typedef ComponentBase*(*ComponentFactoryMethod)(TiXmlNode* p_pNode);
		typedef std::map<std::string, ComponentFactoryMethod> ComponentFactoryMap;

		//---------------------------------------------------------------------
		// Public interface
		//---------------------------------------------------------------------
		GameObjectManager();
		~GameObjectManager();

		GameObject* CreateGameObject();
		void DestroyGameObject(GameObject* p_pGameObject);
		void DestroyAllGameObjects();
		GameObject* GetGameObject(const std::string &p_strGOGUID);
		bool SetGameObjectGUID(GameObject* p_pGameObject, const std::string &p_strGOGUID);

		// Data driven helpers
		GameObject* CreateGameObject(const std::string& p_strGameObject);
		void RegisterComponentFactory(const std::string& p_strComponentId, ComponentFactoryMethod);

		void Update(float p_fDelta);
		void SyncTransforms();

		// Iterator access to allow outside traversal of GameObjects
		GameObjectMap::iterator Begin()	{ return m_mGOMap.begin(); }
		GameObjectMap::iterator End()	{ return m_mGOMap.end(); }

		void AddRemovedObject(GameObject *p_pObject);
		void CheckCollision();
	private:
		

		//---------------------------------------------------------------------
		// Private members
		//---------------------------------------------------------------------
		GameObjectMap m_mGOMap;
		
		// list to be removed next frame
		GameObjectList m_lRemoveGOList;

		ComponentFactoryMap m_mComponentFactoryMap;
	};
} // namespace Common

#endif // GAMEOBJECTMANAGER_H