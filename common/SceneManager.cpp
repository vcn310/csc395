//------------------------------------------------------------------------
// SceneManager
//
// Created:	2012/12/23
// Author:	Carel Boers
//	
// A simple scene manager to manage our models and camera.
//------------------------------------------------------------------------

#include "SceneManager.h"

#include <algorithm>

using namespace Common;

// Static singleton instance
SceneManager* SceneManager::s_pSceneManagerInstance = NULL;

// TODO:
struct DirectionalLight
{
	glm::vec3		m_vDirection;
	wolf::Color4    m_ambient;
	wolf::Color4	m_diffuse;
	wolf::Color4	m_specular;
	DirectionalLight() : m_diffuse(0,0,0,0), m_specular(0,0,0,0), m_ambient(0,0,0,0) {}    
} g_light1;

//------------------------------------------------------------------------------
// Method:    CreateInstance
// Returns:   void
// 
// Creates the singletone instance.
//------------------------------------------------------------------------------
void SceneManager::CreateInstance()
{
	assert(s_pSceneManagerInstance == NULL);
	s_pSceneManagerInstance = new SceneManager();
}

//------------------------------------------------------------------------------
// Method:    DestroyInstance
// Returns:   void
// 
// Destroys the singleton instance.
//------------------------------------------------------------------------------
void SceneManager::DestroyInstance()
{
	assert(s_pSceneManagerInstance != NULL);
	delete s_pSceneManagerInstance;
	s_pSceneManagerInstance = NULL;
}

//------------------------------------------------------------------------------
// Method:    Instance
// Returns:   SceneManager::SceneManager*
// 
// Access to singleton instance.
//------------------------------------------------------------------------------
SceneManager* SceneManager::Instance()
{
	assert(s_pSceneManagerInstance);
	return s_pSceneManagerInstance;
}

//------------------------------------------------------------------------------
// Method:    SceneManager
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
SceneManager::SceneManager()
	:
	m_pCamera(NULL)
{
	// Initialize the light parameters
	g_light1.m_diffuse = wolf::Color4(1.0f,1.0f,1.0f,1.0f);
	g_light1.m_ambient = wolf::Color4(0.1f,0.1f,0.1f,1.0f);
	g_light1.m_specular = wolf::Color4(1.0f,1.0f,1.0f,1.0f);
	g_light1.m_vDirection = glm::vec3(0.0f,0.0f,-1.0f);
}

//------------------------------------------------------------------------------
// Method:    ~SceneManager
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
SceneManager::~SceneManager()
{
}

//------------------------------------------------------------------------------
// Method:    AddModel
// Parameter: wolf::Model * p_pModel
// Returns:   void
// 
// Adds a model the scene manager.
//------------------------------------------------------------------------------
void SceneManager::AddModel(wolf::Model* p_pModel)
{
	m_lModelList.push_back(p_pModel);
}

void SceneManager::AddSquareTexture(Square* p_pSquare)
{
	m_lSquareList.push_back(p_pSquare);
}

void SceneManager::AddPointLight(PointLight* p_pPointLight)
{
	m_lPointLightList.push_back(p_pPointLight);
}

//------------------------------------------------------------------------------
// Method:    RemoveModel
// Parameter: wolf::Model * p_pModel
// Returns:   void
// 
// Removes a model from the scene manager.
//------------------------------------------------------------------------------
void SceneManager::RemoveModel(wolf::Model* p_pModel)
{
	ModelList::iterator it = std::find(m_lModelList.begin(), m_lModelList.end(), p_pModel);
	if (it != m_lModelList.end())
	{
		m_lModelList.erase(it);
	}	
}

void SceneManager::RemoveSquareTexture(Square* p_pSquare)
{
	SquareTextureList::iterator it = std::find(m_lSquareList.begin(), m_lSquareList.end(), p_pSquare);
	if (it != m_lSquareList.end())
	{
		m_lSquareList.erase(it);
	}	
}

void SceneManager::RemovePointLight(PointLight* p_pPointLight)
{
	PointLightList::iterator it = std::find(m_lPointLightList.begin(), m_lPointLightList.end(), p_pPointLight);
	if (it != m_lPointLightList.end())
	{
		m_lPointLightList.erase(it);
	}	
}

//------------------------------------------------------------------------------
// Method:    Clear
// Returns:   void
// 
// Clears the list of models in the scene manager.
//------------------------------------------------------------------------------
void SceneManager::Clear()
{
	m_lModelList.clear();
}

//------------------------------------------------------------------------------
// Method:    AttachCamera
// Parameter: SceneCamera * p_pCamera
// Returns:   void
// 
// Attaches the given camera to the scene
//------------------------------------------------------------------------------
void SceneManager::AttachCamera(SceneCamera* p_pCamera)
{
	m_pCamera = p_pCamera;
}

//------------------------------------------------------------------------------
// Method:    GetCamera
// Returns:   SceneCamera*
// 
// Returns the active camera.
//------------------------------------------------------------------------------
SceneCamera* SceneManager::GetCamera()
{
	return m_pCamera;
}


//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Iterates the list of models, applies the camera params to the shader and 
// renders the model.
//------------------------------------------------------------------------------
void SceneManager::Render()
{
	// Can't render without a camera
	if (m_pCamera == NULL)
	{
		return;
	}

	// Get the view/proj matrices from the camera
	const glm::mat4& mProj = m_pCamera->GetProjectionMatrix();
	const glm::mat4& mView = m_pCamera->GetViewMatrix();

	//PointLight* pPointLight1 = m_lPointLightList.at(0);
    //PointLight* pPointLight2 = m_lPointLightList.at(1);

	

	// Iterate over the list of models and render them
	ModelList::iterator it = m_lModelList.begin(), end = m_lModelList.end();
	for (; it != end; ++it)
	{
		wolf::Model* pModel = static_cast<wolf::Model*>(*it);

		// Set the light parameters
		pModel->GetMaterial()->SetUniform("ViewDir", glm::vec3(0.0f,0.0f,1.0f));
		
		pModel->GetMaterial()->SetUniform("LightDiffuse", g_light1.m_diffuse);
		pModel->GetMaterial()->SetUniform("LightSpecular", g_light1.m_specular);
		pModel->GetMaterial()->SetUniform("LightDir", g_light1.m_vDirection);

		pModel->GetMaterial()->SetUniform("LightAmbient", wolf::Color4(0.5f,0.5f,0.5f,1.0f));


		/*pModel->GetMaterial()->SetUniform("LightPos1", pPointLight1->m_vPosition);
		pModel->GetMaterial()->SetUniform("LightAttenuation1", pPointLight1->m_vAttenuation);
		pModel->GetMaterial()->SetUniform("LightDiffuse1", pPointLight1->m_diffuse);
		pModel->GetMaterial()->SetUniform("LightSpecular1", pPointLight1->m_specular);
		pModel->GetMaterial()->SetUniform("LightRange1", pPointLight1->m_fRange);

		pModel->GetMaterial()->SetUniform("LightPos2", pPointLight2->m_vPosition);
		pModel->GetMaterial()->SetUniform("LightAttenuation2", pPointLight2->m_vAttenuation);
		pModel->GetMaterial()->SetUniform("LightDiffuse2", pPointLight2->m_diffuse);
		pModel->GetMaterial()->SetUniform("LightSpecular2", pPointLight2->m_specular);
		pModel->GetMaterial()->SetUniform("LightRange2", pPointLight2->m_fRange);*/

		pModel->Render(mView, mProj);
	}

	SquareTextureList::iterator it1 = m_lSquareList.begin(), end1 = m_lSquareList.end();
	for (; it1 != end1; ++it1)
	{
		Square* pSquare = static_cast<Square*>(*it1);

		/*pSquare->GetMaterial()->SetUniform("LightPos1", pPointLight1->m_vPosition);
		pSquare->GetMaterial()->SetUniform("LightAttenuation1", pPointLight1->m_vAttenuation);
		pSquare->GetMaterial()->SetUniform("LightDiffuse1", pPointLight1->m_diffuse);
		pSquare->GetMaterial()->SetUniform("LightSpecular1", pPointLight1->m_specular);
		pSquare->GetMaterial()->SetUniform("LightRange1", pPointLight1->m_fRange);

		pSquare->GetMaterial()->SetUniform("LightPos2", pPointLight2->m_vPosition);
		pSquare->GetMaterial()->SetUniform("LightAttenuation2", pPointLight2->m_vAttenuation);
		pSquare->GetMaterial()->SetUniform("LightDiffuse2", pPointLight2->m_diffuse);
		pSquare->GetMaterial()->SetUniform("LightSpecular2", pPointLight2->m_specular);
		pSquare->GetMaterial()->SetUniform("LightRange2", pPointLight2->m_fRange);*/

		pSquare->Render(mView, mProj);
	}

	//render HUD textbox
	m_pTextBox->Render();
}