//------------------------------------------------------------------------
// SceneManager
//
// Created:	2012/12/23
// Author:	Carel Boers
//	
// A simple scene manager to manage our models and camera.
//------------------------------------------------------------------------

#ifndef COMMON_SCENEMANAGER_H
#define COMMON_SCENEMANAGER_H

#include "W_Model.h"
#include "SceneCamera.h"
#include "assignment2/ExampleGame/Square.h"
#include "assignment2/ExampleGame/ComponentLight.h"
#include "assignment2\ExampleGame\TTextBox.h"

namespace Common
{
	class SceneManager
	{
		// Typedef for convenience
		typedef std::vector<wolf::Model*> ModelList;
		typedef std::vector<Square*> SquareTextureList;
		typedef std::vector<PointLight*> PointLightList;


	public:
		//------------------------------------------------------------------------------
		// Public methods.
		//------------------------------------------------------------------------------
		static void CreateInstance();
		static void DestroyInstance();
		static SceneManager* Instance();

		void AddModel(wolf::Model* p_pModel);
		void RemoveModel(wolf::Model* p_pModel);
		void AddSquareTexture(Square* p_pSquare);
		void RemoveSquareTexture(Square* p_oSquare);
		void AddPointLight(PointLight* p_pPointLight);
		void RemovePointLight(PointLight* p_pPointLight);
		void Clear();

		void AttachCamera(SceneCamera* p_pCamera);
		SceneCamera* GetCamera();

		void AttachHUDTextBox(TTextBox *p_pTextBox) {m_pTextBox = p_pTextBox;};
		TTextBox* GetHUDTextBox() {return m_pTextBox;};

		void Render();

	private:
		//------------------------------------------------------------------------------
		// Private methods.
		//------------------------------------------------------------------------------
		
		// Constructor/Destructor are private because we're a Singleton
		SceneManager();
		virtual ~SceneManager();

	private:
		//------------------------------------------------------------------------------
		// Private members.
		//------------------------------------------------------------------------------

		// Static singleton instance
		static SceneManager* s_pSceneManagerInstance;

		// A list of models to render
		ModelList m_lModelList;

		SquareTextureList m_lSquareList;
		PointLightList m_lPointLightList;

		// A camera to view the scene
		SceneCamera* m_pCamera;

		TTextBox* m_pTextBox;
	};

} // namespace common

#endif // COMMON_SCENEMANAGER_H