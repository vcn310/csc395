#include "ComponentLight.h"
#include "SceneManager.h"
#include "GameObject.h"

namespace Common
{
ComponentLight::ComponentLight()
{
	m_pPointLight = new PointLight();
	Common::SceneManager::Instance()->AddPointLight(m_pPointLight);
}

ComponentLight::~ComponentLight(void)
{
	delete m_pPointLight;
}

void ComponentLight::SyncTransform()
{
	m_pPointLight->m_vPosition = this->GetGameObject()->GetTransform().GetTranslation();
}
}